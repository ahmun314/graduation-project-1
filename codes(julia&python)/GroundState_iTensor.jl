using ITensors
#save data libs
using DataFrames
using CSV

function get_Hamiltonian(N, Jxy, Jz)

  sites = siteinds("S=1/2",N)
  
  ampo = OpSum()
  for j=1:N-1 
    ampo += Jz , "Sz",j,"Sz",j+1
    ampo += Jxy, "Sx",j,"Sx",j+1
    ampo += Jxy, "Sy",j,"Sy",j+1
  end

  H = MPO(ampo,sites)
  return H, sites
end

function perform_sweeps(H, psi0, nsweep, bonddim, tuncerr)

  sweeps = Sweeps(nsweep)
  setmaxdim!(sweeps, bonddim)
  setcutoff!(sweeps, truncerr)

  energy, psi = dmrg(H, psi0, sweeps)

  return energy, psi
end


Jxy=1
Jz=1
truncerr=1E-12
nsweep = 5
bonddim= 5
bondlist = 1:2:10

Nlist = 7:10;
Elist=[]
E0s = Vector{Float64}();

e0ss=[];
E0s;

#Gathered in under one for loop 
for N in Nlist
  HH, sites = get_Hamiltonian(N, Jxy, Jz)
  psi0 = randomMPS(sites,10)
  E0, psi = perform_sweeps(HH, psi0, nsweep, bonddim, truncerr)

  push!(Elist,real(E0)/N)
  for bonddim in bondlist
    e0, psi = perform_sweeps(HH, psi0, nsweep, bonddim, truncerr)
    append!(E0s,real(e0)/N)
   end
end 
E0list=[Elist[i][1] for i=1:length(Nlist)];


# save data here, we will do plotting in another file.
iterate=1:length(bondlist):length(E0s)
for i in iterate
    push!(e0ss,E0s[i:i+length(bondlist)-1])
    
end

#saving the data to .csv files
df1 = DataFrame(e07=e0ss[1], e08=e0ss[2], e09=e0ss[3], e010=e0ss[4],Bondd=bondlist) 

CSV.write("data/ITensorE0(Bondim).csv", df1)

df2=DataFrame(E0s=E0list,N=Nlist) 

CSV.write("data/GS(ITensors).csv", df2)




