using PyPlot
using DataFrames
using CSV

#Comparing QOptics&ITensors-GS Energies(N)
datadir = "data/"
GSs=["GS(QO)", "GS(ITensors)"]
GstoDf=Dict{String, DataFrame}()
for g in GSs
    GstoDf[g]=CSV.read( datadir * g * ".csv", DataFrame)
end
energy_column=Symbol("E0s")
site_column=Symbol("N")

outdir = "../figures/"

begin
    g_to_symbol=Dict("GS(QO)" => "o", "GS(ITensors)" => "s")
    g_to_line = Dict("GS(QO)" => ":", "GS(ITensors)" => "-")
    g_to_color=Dict("GS(QO)" => "green", "GS(ITensors)" => "red")
    figure()
    xlabel("N")
    ylabel("E0/N")
    grid("on")
    for g in GSs
        plot(GstoDf[g][:,site_column],GstoDf[g][:,energy_column],marker=g_to_symbol[g],label=g,linewidth = 2,
        color=g_to_color[g],linestyle = g_to_line[g])

    end
    legend(["(QOptics)", "(ITensors)"])
    title("QOptics/ITensors-GS Energies(N)")
    tight_layout()
    savefig( outdir*"GS(QOIT).pdf", bbox_inches = "tight", pad_inches = 0.1 )
end

#Ground State ITensors(Bond dim)
df1=CSV.read("data/ITensorE0(Bondim).csv", DataFrame)
begin
    GSsb=1:(length(df1[:,1])-1)
    figure()
    legend(["E07","E08","E09","E010"])
    xlabel("Bond Dim")
    ylabel("E0/N")
    grid("on")
    
    for b in GSsb
        plot(df1[:,5],df1[:,b],linewidth = 0.8)

    end
    legend(["E07","E08","E09","E010"])
    title("ITensors-GS Energies(BondDim)")
    tight_layout()
    savefig( outdir*"ITensorE0(Bondim).pdf", bbox_inches = "tight", pad_inches = 0.1 )

end

error("stop")

#Comparing QOptics&ITensors-Time Evolution
TEs=["Expect(QO)", "time-evolution"]
TEstoDf=Dict{String, DataFrame}()
for T in TEs
    TEstoDf[T]=CSV.read( T * ".csv", DataFrame)
end

#Sites&times
center=Symbol("m5z")
centerc=Symbol("m6z")
timee=Symbol("TimeOut") 

begin
    Te_to_line = Dict("Expect(QO)" => ":", "time-evolution" => "-")
    figure()
    xlabel("Time")
    ylabel(L"$\langle S^{Z} \rangle$") 
    grid("on")
    for T in TEs
        plot(TEstoDf[T][:,timee],TEstoDf[T][:,center],linestyle = Te_to_line[T])
        plot(TEstoDf[T][:,timee],TEstoDf[T][:,centerc],linestyle = Te_to_line[T])
    end
    legend(["m5z(ITensors)", "m6z(ITensors)","m5z(QOptics)","m6z(QOptics)"])
    title("QOptics/ITensors-TimeEvolution")
    tight_layout()
    savefig( outdir*"time-evolution.pdf", bbox_inches = "tight", pad_inches = 0.1 )
end

#Time-Evolution(ITensors)
df2=CSV.read("longertime-step.csv", DataFrame)
df3=CSV.read("biggercut-off", DataFrame)



#Plot of longer Time-step  
PyPlot.plot(df2[:,3], df2[:,1])
PyPlot.plot(df2[:,3], df2[:,2])
title("Longer Time Scale")
xlabel("Time")
ylabel(L"$\langle  S^{Z}  \rangle$") 
grid("on")
legend(["m5z(ITensors)","m6z(ITensors)"])
savefig( outdir*"Longer Time Scale.pdf", bbox_inches = "tight", pad_inches = 0.1 )

#Plot of bigger cut-off
PyPlot.plot(df3[:,3], df3[:,1])
PyPlot.plot(df3[:,3], df3[:,2])
title(L"Cut-Off($10^{-4}$)")
xlabel("Time")
ylabel(L"$\langle S^{Z} \rangle$") 
grid("on")
legend(["m5z(ITensors)","m6z(ITensors)"])
savefig( outdir*"smallercut-off.pdf", bbox_inches = "tight", pad_inches = 0.1 )

